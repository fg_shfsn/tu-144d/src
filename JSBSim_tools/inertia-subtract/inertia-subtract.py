#!/usr/bin/env python
#
#
# inertia-subtract.py
# Calculate inertia tensor of empty aircraft by subtracting the
# inertia of point masses from tensor of the loaded aircraft.
#
# Thanks to Bomber from FlightGear Discord
#
#
# USAGE
#
# Edit the files in the "input" directory (see the files for
# instructions / examples), run the script. Any units can be used for
# inputs as long as they are consistent, the output will be in the
# same units.
#
# Run the script, and it will print the resulting tensor. Only the
# upper triangle is used.
#
# The input is printed to stderr, so that you can check if your inputs
# have been read correctly.


import sys
import math as m
import numpy as np

I = np.loadtxt("input/I.txt")
# Overwrite lower triangle with upper triangle, just in case:
#ii_lower = np.tril_indices(3, -1)
#I[ii_lower] = I.T[ii_lower]
print("Inertia tensor:", file=sys.stderr)
print(I, file=sys.stderr)
print("\n", file=sys.stderr)

CG = np.loadtxt("input/CG.txt")
CG = CG.T
print("CG location:", file=sys.stderr)
print(CG, file=sys.stderr)
print("\n", file=sys.stderr)

loads = np.loadtxt("input/loads.txt")
print("Loads:", file=sys.stderr)
print(loads, file=sys.stderr)
print("\n", file=sys.stderr)

for load in loads:
 pos = load[:3]
 mass = load[3]
 arm = pos - CG
 arm2 = arm * arm
 I_load = np.array([
  [arm2[1] + arm2[2], -arm[0] * arm[1],  -arm[0] * arm[2] ],
  [0.0,               arm2[0] + arm2[2], -arm[1] * arm[2] ],
  [0.0,               0.0,               arm2[1] + arm2[1]]
 ]);
 I_load *= mass
 I -= I_load

np.savetxt(sys.stdout, I, fmt="%.3f", delimiter="\t")
print()
print(' <ixx unit="REPLACEME">{:.3f}</ixx>'.format(I[0][0]))
print(' <iyy unit="REPLACEME">{:.3f}</iyy>'.format(I[1][1]))
print(' <izz unit="REPLACEME">{:.3f}</izz>'.format(I[2][2]))
print(' <ixy unit="REPLACEME">{:.3f}</ixy>'.format(I[0][1]))
print(' <ixz unit="REPLACEME">{:.3f}</ixz>'.format(I[0][2]))
print(' <iyz unit="REPLACEME">{:.3f}</iyz>'.format(I[1][2]))
