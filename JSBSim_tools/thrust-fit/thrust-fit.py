#!/usr/bin/env python
#
#
# thrust-fit.py
# Fit Aeromatic++ thrust tables to known experimental points for your
# engine.
#
#
# USAGE
#
# Edit the files "input/bypassratio.txt", "input/points_milthrust.txt"
# and "input/points_augthrust.txt" according to comments given in the
# files.
#
# REMARKS.
# 1. If the Mach number and altitude of data points is outside of
#    tables' range, they will be extrapolated.
# 2. Ceiling altitude was removed from the input tables. The author
#    believes the right way of implementing such limits is via RPM,
#    temperatures and compressor stall in your engine system file.
#
#
# TECHNICAL DETAILS
#
# As the first step this program basically does the same thing as
# Aeromatic++: take the precomputed tables and adjust MilThrust for
# user-provided bypass ratio. Next it scales each altitude to fit with
# user's data points.
#
# Aeromatic++ data is based on JSBSim
# utils/aeromatic++/Systems/Propulsion.cpp as of 18 Nov 2018.


# Make printing work in Python 2
from __future__ import print_function

import sys
import math as m
import numpy as np
np.set_printoptions(linewidth=999)
from scipy import interpolate as interp

bypassratio = np.loadtxt("input/bypassratio.txt")

idl = np.loadtxt("aeromatic_data/idlethrust.txt")
mil = np.loadtxt("aeromatic_data/milthrust.txt")
aug = np.loadtxt("aeromatic_data/augthrust.txt")

# First row, except the first value, is the altitude.
idl_alt = idl[0, 1:]
mil_alt = mil[0, 1:]
aug_alt = aug[0, 1:]
# First column, except the first value, is the Mach number.
idl_mach = idl[1:, 0]
mil_mach = mil[1:, 0]
aug_mach = aug[1:, 0]
# The rest of the table is thrust.
idl = idl[1:, 1:]
mil = mil[1:, 1:]
aug = aug[1:, 1:]
# Scaling of military thrust for bypass ratio (see Aeromatic++).
for imach in range(0, mil_mach.shape[0]):
 mach = mil_mach[imach]
 mil[imach] *= (1.0 - 0.11 * mach * bypassratio)
idl_interp = interp.interp2d(idl_alt, idl_mach, idl)
mil_interp = interp.interp2d(mil_alt, mil_mach, mil)
aug_interp = interp.interp2d(aug_alt, aug_mach, aug)

mil_points = np.loadtxt("input/milthrust.txt")
aug_points = np.loadtxt("input/augthrust.txt")
mil_scale = np.zeros(shape=mil_points.shape[0])
aug_scale = np.zeros(shape=aug_points.shape[0])
idl_new_alt = np.copy(idl_alt)
idl_new_mach = np.copy(idl_mach)
mil_new_alt = np.copy(mil_alt)
mil_new_mach = np.copy(mil_mach)
aug_new_alt = np.copy(aug_alt)
aug_new_mach = np.copy(aug_mach)
# Calculate per-altitude scaling for milthrust and idlethrust.
ii = 0
for point in mil_points:
 mach = point[0]
 alt = point[1]
 expthrust = point[2]
 scale = expthrust / mil_interp(alt, mach)
 mil_scale[ii] = scale
 # Always insert the altitudes in the table.
 index = np.searchsorted(mil_new_alt, alt, side="right")
 if(
  (index == 0) or
  (index > mil_new_alt.shape[0] - 2) or
  (alt - mil_new_alt[index - 1] > 0.5) and
  (alt - mil_new_alt[index] < -0.5)
 ):
  mil_new_alt = np.insert(mil_new_alt, index, alt)
 index = np.searchsorted(idl_new_alt, alt, side="right")
 if(
  (index == 0) or
  (index > idl_new_alt.shape[0] - 2) or
  (alt - idl_new_alt[index - 1] > 0.5) and
  (alt - idl_new_alt[index] < -0.5)
 ):
  idl_new_alt = np.insert(idl_new_alt, index, alt)
 # Insert Mach numbers only if they are out of range.
 index = np.searchsorted(mil_new_mach, mach, side="right")
 if(
  (index == 0) or
  (index > mil_new_mach.shape[0] - 2)
 ):
  mil_new_mach = np.insert(mil_new_mach, index, mach)
 index = np.searchsorted(idl_new_mach, mach, side="right")
 if(
  (index == 0) or
  (index > idl_new_mach.shape[0] - 2)
 ):
  idl_new_mach = np.insert(idl_new_mach, index, mach)
 ii += 1
mil_scale_interp = interp.interp1d(mil_points[:, 1], mil_scale, fill_value="extrapolate")
# Calculate per-altitude scaling for augthrust.
ii = 0
for point in aug_points:
 mach = point[0]
 alt = point[1]
 expthrust = point[2]
 scale = expthrust / aug_interp(alt, mach)
 aug_scale[ii] = scale
 # Always insert the altitudes in the table.
 index = np.searchsorted(aug_new_alt, alt, side="right")
 if(
  (index == 0) or
  (index > aug_new_alt.shape[0] - 2) or
  (alt - aug_new_alt[index - 1] > 0.5) and
  (alt - aug_new_alt[index] < -0.5)
 ):
  aug_new_alt = np.insert(aug_new_alt, index, alt)
 # Insert Mach numbers only if they are out of range.
 index = np.searchsorted(aug_new_mach, mach, side="right")
 if(
  (index == 0) or
  (index > aug_new_mach.shape[0] - 2)
 ):
  aug_new_mach = np.insert(aug_new_mach, index, mach)
 ii += 1
aug_scale_interp = interp.interp1d(aug_points[:, 1], aug_scale, fill_value="extrapolate")

print("IdleThrust:")
for alt in idl_new_alt:
 print("\t{0:.0f}".format(alt), end='')
print()
for mach in idl_new_mach:
 print("{0:.1f}".format(mach), end='')
 for alt in idl_new_alt:
  print("\t{0:.4f}".format(mil_scale_interp(alt) * idl_interp(alt, mach)[0]), end='')
 print()
print("MilThrust:")
for alt in mil_new_alt:
 print("\t{0:.0f}".format(alt), end='')
print()
for mach in mil_new_mach:
 print("{0:.1f}".format(mach), end='')
 for alt in mil_new_alt:
  print("\t{0:.4f}".format(mil_scale_interp(alt) * mil_interp(alt, mach)[0]), end='')
 print()
print("AugThrust:")
for alt in aug_new_alt:
 print("\t{0:.0f}".format(alt), end='')
print()
for mach in aug_new_mach:
 print("{0:.1f}".format(mach), end='')
 for alt in aug_new_alt:
  print("\t{0:.4f}".format(aug_scale_interp(alt) * aug_interp(alt, mach)[0]), end='')
 print()
