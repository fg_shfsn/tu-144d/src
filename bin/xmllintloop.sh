#!/bin/sh --

JOBS=${JOBS:-$(nproc)}

clear
find . -iname \*.xml -print0 |xargs -0 -P${JOBS} xmllint --noout
echo
echo Press Return to check again.

while read a; do
 clear
 find . -iname \*.xml -print0 |xargs -0 -P${JOBS} xmllint --noout
 echo
 echo Press Return to check again.
done
