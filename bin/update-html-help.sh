#!/bin/sh --

JOBS=${JOBS:-$(nproc)}

find . -iname \*.md -print0 |xargs -0 -P${JOBS} -I {} \
 pandoc --standalone --from markdown_strict+yaml_metadata_block --to html {} -o {}.html
