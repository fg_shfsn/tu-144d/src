#!/bin/sh --
set -e

# WONTFIX I tried hard to stop this from choking on spaces, but it
# WONTFIX still does.

FILELIST=${@:?}
TMPDIR=$(mktemp -d)

for file in ${FILELIST}; do
 tmpname="${TMPDIR}/$(basename ${file})"
 convert "${file}" -background black -alpha remove "${tmpname}"
 mv -f "${tmpname}" "${file}"
done

trap "rm -rf ${TMPDIR:?}" 0 2 9 15
