#!/usr/bin/gnuplot -c
# [Затучный А.М., Ригмант В.Г., Синеокий П.М.. Туполев-144 // Знаменитые летательные аппараты, p. 397]

set xlabel "M"
set xtics (0.0, 0.4, 0.68, 0.95, 1.23, 1.5, 2.0)
set ylabel "X̄_т, % САХ"

set key right bottom
set style line 12 lc black lw 1
set grid xtics ytics mxtics mytics ls 12, ls 12

set term svg
set output "picture.svg"
plot \
 "limits.txt" u 1:2 w l t "Предельные центровки" lw 4 lc black, \
 "typical.txt" u 1:2 w l t "Эксплуатационный диапазон" lw 2 lc black dt "-..-", \
 "fullclimb.txt" u 1:2 w l t "Прибл. набор с полн. заправкой" lw 2 lc black

set term pdfcairo
set output "picture.pdf"
plot \
 "limits.txt" u 1:2 w l t "Предельные центровки" lw 4 lc black, \
 "typical.txt" u 1:2 w l t "Эксплуатационный диапазон" lw 2 lc black dt "-..-", \
 "fullclimb.txt" u 1:2 w l t "Прибл. набор с полн. заправкой" lw 2 lc black
