sox -r44100 -b16 -n bell.wav \
 synth 1.0 sine 1300.0 \
 synth 1.0 sine mix 3600.0 \
 synth 1.0 sine mix 4200.0 \
 synth 1.0 sawtooth amod 50.0 38.0 25.0
