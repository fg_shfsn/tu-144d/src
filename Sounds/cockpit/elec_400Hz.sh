sox -r44100 -b16 -n elec_400Hz.wav \
 synth 1.0 square 400.0 \
 vol 0.5 \
 synth 1.0 square mix 800.0 \
 synth 1.0 sine mix 2400.0 \
 synth 1.0 sine mix 1600.0
