# FIXME Make them "RSB" and "KAT".

# T
sox -r44100 -b16 -n mPRMG.wav \
 synth 0.45 sine 1050.0 \
 pad 8.0@0.45

# E
sox -r44100 -b16 -n mRSBN.wav \
 synth 0.15 sine 1050.0 \
 pad 8.0@0.15
