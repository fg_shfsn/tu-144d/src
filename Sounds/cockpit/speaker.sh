sox -r44100 -b16 -n speaker.wav \
 synth 0.4 noise \
 vol 0.25 \
 synth 0.4 square mix 400.0 \
 synth 0.4 sine mix 5200.0 \
 synth 0.4 sine mix 4400.0 \
 synth 0.4 sine mix 1200.0

sox -r44100 -b16 -n speaker_int.wav \
 synth 0.4 noise \
 vol 0.25 \
 synth 0.4 square mix 400.0 \
 synth 0.4 sine mix 5200.0 \
 synth 0.4 sine mix 4400.0 \
 synth 0.4 sine mix 1200.0 \
 fade t 0.0 0.30 0.05 \
 pad 0.1@0.30
