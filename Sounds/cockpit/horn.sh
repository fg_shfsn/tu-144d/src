sox -r44100 -b16 -n horn.wav \
 synth 0.4 noise \
 synth 0.4 sine mix 2800.0 \
 synth 0.4 sine mix 2400.0 \
 synth 0.4 sine mix 2000.0 \
 synth 0.4 sine amod 200.0 38 25 \
 synth 0.4 triangle mix 200.0

sox -r44100 -b16 -n horn_int.wav \
 synth 0.3 noise \
 synth 0.3 sine mix 2800.0 \
 synth 0.3 sine mix 2400.0 \
 synth 0.3 sine mix 2000.0 \
 synth 0.3 sine amod 200.0 38 25 \
 synth 0.3 triangle mix 200.0 \
 fade t 0.0 0.30 0.05 \
 pad 0.1@0.30
