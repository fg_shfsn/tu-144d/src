#!/bin/sh --

# gimmedat
# Extract a particular table from DATCOM output file
#
# WARNING. This is a throwaway program that uses as input what was not
# meant to be machine readable in the first place, and as such it will
# not work with arbitrary input file. Most likely you will have to
# adapt it, so I left out any sanity checks and error messages to keep
# the code manageable.
#
#
# REQUIREMENTS
#
# pcregrep
#
#
# USAGE
#
# gimmedat <output.dat> <CASEID> <NALPHA> <mach> <alt> <command> <cmdarg>
#
# mode         description               cmdarg
# static       static coefficients       name of configuration
# dynamic      dynamic derivatives       name of configuration
# symflp_dCm   wing symmetric flap dCm   NDELTA
# symflp_dCDi  wing symmetric flap dCDi  NDELTA
# asyflp_Cn    wing asymmetric flap Cn   NDELTA
# asyflp_Cl    wing asymmetric flap Cl   NDELTA
#
#
# EXAMPLES
#
# $ gimmedat work/Tu-144D_equivalent/output.dat 'Tu-144D' 20 2.0 10000.0 static 'WING-BODY-VERTICAL TAIL'
#
# $ gimmedat work/Tu-144D/output.dat 'Tu-144D cone 11.5' 20 0.5 0.0 dynamic WING-BODY
#
# $ gimmedat work/Tu-144D/output.dat 'Tu-144D' 20 0.5 10000.0 symflp_dCm 9
#
# $ gimmedat work/Tu-144D/output.dat 'Tu-144D' 20 0.5 10000.0 symflp_dCDi 9
#
# $ gimmedat work/Tu-144D/output.dat 'Tu-144D aileron' 20 0.5 10000.0 asyflp_Cn 9
#
# $ gimmedat work/Tu-144D/output.dat 'Tu-144D aileron' 20 0.5 10000.0 asyflp_Cl 9


set -e

FILE=${1:?}
CASE=${2:?}
NALPHA=${3:?}
MACH=${4:?}
ALT=${5:?}
COMMAND=${6:?}
ARG=${7:?}

static()
{
 pcregrep -M -A$((NALPHA+9)) " CHARACTERISTICS AT ANGLE OF ATTACK AND IN SIDESLIP\s+${ARG} CONFIGURATION\s+${CASE}\$" "$FILE" \
 | pcregrep -M -A$((NALPHA+3)) "^  MACH.*\s^ NUMBER.*\s^              M.*$\s^0 ${MACH}0*  *${ALT}0* " \
 | awk "NR > 7 && NR < $((NALPHA+8))" \
 | sed \
  -e's/                                          /    BLANK        BLANK        BLANK       /g' \
  -e's/                             /    BLANK        BLANK       /g'
}

dynamic()
{
 pcregrep -M -A$((NALPHA+10)) " DYNAMIC DERIVATIVES\s+${ARG} CONFIGURATION\s+${CASE}\$" "$FILE" \
 | pcregrep -M -A$((NALPHA+4)) "^  MACH.*\s^ NUMBER.*\s^              M.*$\s^0 ${MACH}0*  *${ALT}0* " \
 | awk "NR > 8 && NR < $((NALPHA+9))" \
 | sed 's/                                                        /    BLANK       BLANK            BLANK        BLANK     /g'
}

symflp_dCm()
{
 pcregrep -M -A$((ARG+9)) " CHARACTERISTICS OF HIGH LIFT AND CONTROL DEVICES\s+WING PLAIN TRAILING-EDGE FLAP CONFIGURATION\s+${CASE}\$" "$FILE" \
 | pcregrep -M -A$((ARG+4)) "^  MACH.*\s^ NUMBER.*\s^              M.*$\s^0 ${MACH}0*  *${ALT}0* " \
 | awk "NR > 8 && NR < $((ARG+9))"
}

symflp_dCDi()
{
 pcregrep -M -A$((ARG+NALPHA+15)) " CHARACTERISTICS OF HIGH LIFT AND CONTROL DEVICES\s+WING PLAIN TRAILING-EDGE FLAP CONFIGURATION\s+${CASE}\$" "$FILE" \
 | pcregrep -M -A$((ARG+NALPHA+10)) "^  MACH.*\s^ NUMBER.*\s^              M.*$\s^0 ${MACH}0*  *${ALT}0* " \
 | awk "NR == $((ARG+12)) || (NR > $((ARG+14)) && NR < $((ARG+NALPHA+15)))" \
 | sed 's/^0       DELTA =   /                  /'
}

asyflp_Cn()
{
 pcregrep -M -A$((NALPHA+9)) " CHARACTERISTICS OF HIGH LIFT AND CONTROL DEVICES\s+WING PLAIN TRAILING-EDGE FLAP CONFIGURATION\s+${CASE}\$" "$FILE" \
 | pcregrep -M -A$((NALPHA+4)) "^  MACH.*\s^ NUMBER.*\s^              M.*$\s^0 ${MACH}0*  *${ALT}0* " \
 | awk "NR == 6 || (NR > 8 && NR < $((NALPHA+9)))" \
 | sed 's/^0(DELTAL-DELTAR)= /                  /'
}

asyflp_Cl()
{
 pcregrep -M -A$((NALPHA+ARG+12)) " CHARACTERISTICS OF HIGH LIFT AND CONTROL DEVICES\s+WING PLAIN TRAILING-EDGE FLAP CONFIGURATION\s+${CASE}\$" "$FILE" \
 | pcregrep -M -A$((NALPHA+ARG+7)) "^  MACH.*\s^ NUMBER.*\s^              M.*$\s^0 ${MACH}0*  *${ALT}0* " \
 | awk "NR > $((NALPHA+11)) && NR < $((NALPHA+ARG+12))"
}

${COMMAND:?}
