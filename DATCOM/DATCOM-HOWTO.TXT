INTRODUCTION

This is not very short, unfortunately, but likely the fastest guide to
getting the best possible DATCOM results from your 3D model. I based
it on my own knowledge from calculating the Tu-144 -- so if facts or
your experience turn out to be different, don't hesitate to point it
out.

The process is not simple A to B, it is self-consistent. You have to
start with as many Mach, AoA and control deflection data points as you
can (possibly having to run the case several times with different
inputs if their counts are more than allowed):
- first you have to check the outputs each time when adding components
  to your model to make sure nothing strange happens;
- when the model is complete, you have to move/delete data points to
  have most information per number of data points.

I will be referring a lot to Digital DATCOM manual Vol. 1 (further
simply "the manual"), because that is a lot safer than trying to
quote/rephrase, risking extra mistakes. The link is in "REQUIREMENTS".

Please avoid skipping steps, because the workload of going back tends
to grow exponentially over time.

Document any adjustments you make to your data -- right away, before
you forget -- so that developers who come after you do not have to
guess or start over.

To make your model optimal, approximate all curves you encounter in
the process -- like input shapes or selection of output points -- with
the fewest points necessary: start with a point at each sharp cusp
then add a point between them such that the area between the curve and
your approximation is the same to the left and right of the new point,
and in turn add points in between those points the same way.

Unless asked to do otherwise, work in temporary locations. Make a
backup for each step and use version control. Because in many cases,
such as Blender files, the changes can not be rolled back easily, if
at all.



REQUIRED BACKGROUND

1. Some version control system that can handle binary files.

2. Blender:
- keyboard shortcuts for precision work;
- snaps.

3. UNIX:
- Bourne shell (or anything else that you can use for throwing a lot
  of text files around automatically);
- text tools like awk, sed, grep, sort, expand, etc. (you don't have
  to write anything from scratch with them, just understand what
  examples do and be able to adjust them for your data arrangement);
- basic regex, very basic PCRE;
- editing space-separated text tables;
- plotting, e.g. gnuplot or matplotlib;

4. Math tools:
- when your aircraft has unusual moving parts, NumPy or similar will
  be required.


REQUIRED SOFTWARE

1. Patched DATCOM

https://github.com/arktools/aircraft-datcom

2. DATCOM manual Vol. 1

https://github.com/arktools/aircraft-datcom/blob/master/doc/Users'_Manual%20Vol_1.pdf

3. datcom-modeler

https://github.com/jentron/datcom-modeler

CAUTION. Apply the patch "datcom-modeler.patch" given with this
example to fix displaying of double delta wing

4. Blender with the following plugins:

- AC3D I/O

https://github.com/NikolaiVChr/Blender-AC3D

- 3D Print Toolbox;

- Measureit.



CAPTURING INPUTS


1. Read the manual:
- check Table 2 and Section 6.1 to find out what results you can get in
  principle;
- read Table 8 and Figure 4 and choose units and your aircraft's
  dimensions;
- carefullly read Sections 2-3;
- quickly skim through Sections 4 and 5 and Figures 5-22, we will get
  back to them as needed.

There are a few examples in Section 7. Don't take Figure 29 too
seriously, though, later we will find out why!


2. Limitations of DATCOM input that were not explained in the manual.

2.1. In many regimes DATCOM treats aircraft body as simple body of
rotation -- for the best results you have to shift your 3D model
vertically, so that its fuselage axis is at zero ordinate. Contrary,
axial location of the aircraft can be anything, negative abscissas are
ok.

2.1.a. If shifting the 3d model is not much work, it is wise to make
the vertical shift permanent. It is good to shift longitudally so that
the center of the model is above/below typical CG to improve the outer
view in simulators such as FlightGear.

2.1.b. Otherwise, save the shifted version in a temporary file and
DOCUMENT THE SHIFT(S) IN A VISIBLE COMMENT IN THE DATCOM INPUT FILE.

2.2. Limit of 80 characters per line for the input. For extra safety,
I would keep it under 70 CPL.

2.3. No native support for comments. Because comments are absolutely
crucial, we have to find a way around this. I suggest making comments
start with any number of spaces and an asterisk "*". Then the command

$ grep -v "^ *\*"

should produce an input file with which you can run DATCOM, for
example:

$ grep -v "^ *\*" Tu-144D.dcm >Tu-144D-runme.dcm
$ datcom Tu-144D-runme.dcm


3. Namelist OPTINS and general setup.

3.1. Check the manual again:
- Figure 4 for OPTINS inputs;
- Table 8 for unit definition throughout the case;
- Section 3.5 for selection of derivative units (DERIV RAD, DERIV DEG).

3.2. Fill OPTINS card with chosen dimensions.

A T T E N T I O N !  Despite Figure 4 saying you can skip aircraft
dimension input -- set them! Else every time you will have to look
them up in the output, moreover they will end up being totally
different for rudder calculations!

3.3. Add control cards for chosen unit system and derivatives.


4. Namelist FLTCON.

4.1. Read the manual:
- Figure 3.

4.2. Fill in ALSCHD(1) and MACH(1) with as many points as you can,
later you will go back and pick the most important ones.

NOTES:
1. Most likely you will only need a setup with fixed altitude, because
dynamic pressure change with speed will already be accounted FDM-side.
Use LOOP= 2.0, NALT= 1.0 and, for example, ALT(1) = 10000.0. Later you
can go back and change the altitude to see if anything changes much.
2. WT only seems to be needed for level flight CL.
3. If you do not have the experimental results that are required by
transonic methods, you have to disable them with STMACH and TSMACH and
pay attention to the results.


5. Running and results extraction.

It is best to make a setup that puts the results together in a form
that you can plot and use in your FDM as early as possible, because
then you can check if a new part behaves not as you would expect.

5.1. Set up a Makefile or a script that you can simply call to run all
your DATCOM cases at once and/or generate .ac models (example given in
"Makefile"). This will save a lot of time having to remember commands.

5.1. Extracting of per-configuration per-condition tables.

See "bin/gimmedat.sh". You may have to edit/expand it to suit your
purposes, if you have managed to make it more robust or scalable,
feel free to submit improvements to the author.

5.2. Putting tables together for plotting and JSBSim.

This task is very aircraft-specific by its nature, and you will have
to compare a lot of tables from 5.1. before you settle on what you
need in the final model, but you can already start adapting the
scripts under "results_*/bin".

5.3. When adding components;
- check the .ac result of datcom-modeler against your 3D model;
- compare the AoA plots of before and after for a few Mach numbers.

NOTES:
1. Make sure to check "README.TXT" and example .dcm files here,
because they have a few tricks which would be hard to explain out of
context.
2. datcom-modeler does NOT show:
- wing incidence and twist;
- flaps;
- engine power;
- probably many other things.

5.4. When the model is complete:
- compare AoA plots of all coefficients for a few Mach numbers and
adjust the AoA points in FLTCON;
- plot at least CD, CL and Cm for typical AoA for as many Mach numbers
  are you can and adjust FLTCON Mach points.

NOTES:
1. If you are modelling supersonic aircraft, it is likely likely you
will have to throw out Mach range of around 0.69 < M < 1.1 (see
explanation in the bug list in "README.TXT" and use something like
"results_*/bin/prepare.sh" together with
"results_*/bin/plot_static.sh" to check Mach behaviour at fixed AoA).
2. If your aircraft has unusual moving parts which you have modeled by
using separate case per position, calculate both absolute and relative
change of the coefficient tables per position with something like
NumPy. Sometimes the only change of coefficient will be the last digit
due to some rounding differences, so for that coefficient it may be
safe to throw out the dependency on that moving part from the FDM
table. Same applies if the coefficient changes only a very small
fraction.


6. Namelist BODY.

6.1. Read the manual:
- Figure 5 for important coordinate definitions;
- Figure 6, Section 4.1 and 4.2 for required inputs.

Check folder "reference/sections/BODY" and BODY namelist of
"Tu-144D.dcm" for example end results of what you are going to do.

NOTES:
1. Contrary to what Figure 6 says about required S, P, R, it is best
to give all of them.
2. If your aircraft's body can change its shape, you have to repeat
this for each shape. You can save a lot of time if you agree on the
same number of stations for all shapes and capture the common parts
which do not move just once and put an extra station at the break
point. Organise DATCOM cases so that a sequence of cases replace
namelist BODY with that of the other shape (example in "Tu-144D.dcm").

6.2. Copy the aircraft's body to a temporary Blender file, add objects
consisting of two connected verts: one at the beginning of the
aircraft body, the other at the end, with Shrinkwrap modifier to
project them onto the body:
- "ZU" above the body centerline, project vertically;
- "ZL" below the body centerline, project vertically;
- "R"  to the left of the body's maximum width, project laterally.

Cut verts in ZU, ZL and R and snap the verts of R vertically to the
maximum width while keeping their abscissas in sync. USE NO MORE THAN
ONE POINT FEWER THAN MAXIMUM ALLOWED COUNT, BECAUSE YOU WILL NEED ONE
EXTRA POINT LATER. As result you should have lines that represent
upper and lower ordinates and maximum width of the body. When you are
done, apply the Shrinkwrap modifier and, if you need to, tweak the
points manually. Bring all points of R to the ordinate of the body
axis.

6.3. Add an object without verts, named "S_P" At each of the
longitudal points you have defined, inside this object add an ngon
representing section of the fuselage, by either applying boolean
modifiers to the body and throwing out everything but the outer verts
of each station, or simply stretching a loop of verts around the body.
You should now have a set of approximate cross sections.

6.4. When you are absolutely sure about everything, check once more,
ESPECIALLY THAT ABSCISSAS OF EACH STATION MATCH, then copy resulting
ZU, ZL, R and S_P into separate .blend and save it in your repository.

6.5. Export ZU, ZL and R each into a separate .ac file with respective
name. You can extract the coordinates from .ac files with awk and sed
or a text editor.

CAUTION. Depending on the version of AC exporter, Blender, axes used,
weather on Mars etc., resulting X, Y, Z coordinates in exported files
may be switched around or have different sign, please check the
results and adjust the awk's {print blablabla} accordingly.

Convert "ZU.ac" and "ZL.ac" to the text tables "ZU.txt" and "ZL.txt",
assuming X is column 1 and Z is column 2 of .ac:

$ for name in ZU ZL; do awk '/numvert/{printme=1; next}/kids/{printme=0} printme {print $1"\t"$2}' ${name}.ac |sort -g |expand -t11 > ${name}.txt; done

Convert "R.ac" to "R.txt", assuming X is column 1 and Y is column 3 of
.ac. If the values of R end up negative, replace "$3" with "(-$3)":

$ awk '/numvert/{printme=1; next}/kids/{printme=0} printme {print $1"\t"$3}' R.ac |sort -g |expand -t11 > R.txt

6.6. Split out each section from S_P and measure the following values
while putting their results in a text table after abscissa (you can
use a copy of e.g. "ZU.txt" and delete second column):
- the surface areas of each with 3D Print Toolbox plugin, save in
  "S.txt";
- the peripheries with Measureit plugin, save in "P.txt".

6.7. CHECK THAT THE FIRST COLUMNS OF YOUR .txt FILES MATCH, for
example:

$ vimdiff -R ZU.txt ZL.txt R.txt S.txt P.txt

Save "ZU.txt" "ZL.txt" "R.txt" "S.txt" "P.txt" in your repository.

Finally, convert the arrays to DATCOM format and save to "inputs.txt",
which you can use in the BODY namelist:

$ ( printf 'X(1)= '; awk '{printf $2", "}' ZU.txt; echo; for name in S P R ZU ZL; do printf ${name}"(1)= "; awk '{printf $2", "}' ${name}.txt; echo; done ) |sed 's/  *$//' >inputs.txt


7. Namelist WGSCHR, HTSCHR VTSCHR or NACA control card

It is best to use NACA control cards, but if your wing section is
unknown you have to use coordinate input.

7.1. Read the manual:
- Figure 8 and unnamed table that is 2 pages before it for input
  requirements;
- Section 2.4.4 for requirements to the data.

FIXME Explain capture.


8. Namelist WGPLNF, HTPLNF, VTPLNF

8.1. Read the manual:
- Figure 5 for position and incidence;
- Figure 7 (2 pages: the one with drawing, then skip one page and
  there is explanation of required inputs).

8.2. Open your 3d model, add a quad poly for each airfoil --
horizontal for wing and htail, vertical for vtail. If the planform
is double delta, cut one edge in the middle. Stretch it to resemble
Figure 7 while keeping longitudal edges parallel to the aircraft's
axis. If any numeric values are known, try to use them and check if
they match the 3d model.

Model the dihedral by moving the longitudal edges up and down.

It is safer to use CHSTAT= 0.0 and measure leading edge sweep instead
of measuring it elsewhere, unless you already know the numeric values.

You can measure angles in Blender with enough precision by duplicating
one vert, extruding it along an axis and rotating the edge around this
vert and another axis to match the line.

8.3. Approximating double delta wing with straight tapered planform.

In the following cases you have to make a second model with straight
tapered wing planform:
- If your wing is twisted. Wing twist input TWISTA does not seem to
work for double delta wing. Model an equivalent single delta with true
ALIW and TWISTA and then adjust the double delta's ALIW.
- If you will calculate supersonic regimes. Supersonic lift is
unavailable for double delta.

The way of approximating the wing shown on Figure 29 of the manual is
not guaranteed to give the best matching characteristics. In my own
experience, choosing the leading edge sweep so that the wing area is
the same yields the best match of CL and CD. Example: compare WGPLNF
and XW, ZW in namelist SYNTHS of "Tu-144D.dcm" and
"Tu-144D_equivalent.dcm"

Adjust the equivalent wing while checking wing-only CL and CD results
for of both models in the same subsonic Mach.

Adjust the ALIW of double delta until its noticeable Cm changes are at
the same AoA as the one with equivalent wing. Keep in mind CL and CD
will shift as well.


9. Namelist SYMFLP

FIXME


10. Namelist ASYFLP

FIXME
