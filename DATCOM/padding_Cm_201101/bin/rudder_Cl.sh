#!/bin/sh --
#
# Generate JSBSim table for rudder Cl.
# row:    deflection difference, deg
#
set -e


M=0.4
(
 printf "0.0\t0.0\n"
 awk '{printf 0.5*($1-$2)"\t"$3"\n"}' Tu-144D-vtail/aileron/$M/Cl
) |expand -t11
