#!/bin/sh --
#
# Generate JSBSim table for Cnp.
# row:    AoA, deg.
# column: Mach
#
# TODO Try extracting supersonic without vtail?
#
set -e

TMPDIR=`mktemp -d`
trap "rm -rf ${TMPDIR:?}" 0 2 9 15

subMs=$(ls -1 Tu-144D/cone_0.0)
# TODO
superMs=
#superMs=$(ls -1 Tu-144D_equivalent/stability)


nextM=
for M in $subMs; do
 if [ -z "$nextM" ]; then
  awk '{printf $1"\n"}' Tu-144D/cone_0.0/$M/d_wbv >$TMPDIR/AoAs
 fi
 awk '{printf $8"\n"}' Tu-144D/cone_0.0/$M/d_wbv >$TMPDIR/$M
 nextM=1
done

for M in $superMs; do
 awk '{printf $8"\n"}' Tu-144D_equivalent/stability/$M/d_wb >$TMPDIR/$M
done

(
 cd $TMPDIR
 printf '\t'
 echo $subMs $superMs |sed -e's/  */	/g'
 paste AoAs $subMs $superMs
)| expand -t 11
