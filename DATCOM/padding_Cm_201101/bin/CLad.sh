#!/bin/sh --
#
# Generate JSBSim table for CLad.
# row: Mach
#
# FIXME No results in subsonic.
#
set -e

subMs=$(ls -1 Tu-144D/cone_0.0)
superMs=$(ls -1 Tu-144D_equivalent/stability)

for M in $subMs; do
 printf $M
 awk 'NR==1 {printf "\t"$4"\n"}' Tu-144D/cone_0.0/$M/d_wbv
done |expand

for M in $superMs; do
 printf $M
 awk 'NR==1 {printf "\t"$4"\n"}' Tu-144D_equivalent/stability/$M/d_wbv
done |expand
