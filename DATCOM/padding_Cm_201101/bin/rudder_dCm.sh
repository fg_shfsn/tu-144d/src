#!/bin/sh --
#
# Generate JSBSim table for rudder dCm.
# row:    deflection, deg
#
set -e


M=0.4
awk '{printf $1"\t"$3"\n"}' Tu-144D-vtail/elevator/$M/CL_Cm |sort -g |expand
