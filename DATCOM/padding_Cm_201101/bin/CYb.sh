#!/bin/sh --
#
# Generate JSBSim table for CYb.
# row: Mach
#
set -e

subMs=$(ls -1 Tu-144D/cone_0.0)
superMs=$(ls -1 Tu-144D_equivalent/stability)

for M in $subMs; do
 printf $M
 awk 'NR==1 {printf "\t"$10"\n"}' Tu-144D/cone_0.0/$M/s_wbv
done |expand

for M in $superMs; do
 printf $M
 awk 'NR==1 {printf "\t"$10"\n"}' Tu-144D_equivalent/stability/$M/s_wbv
done |expand
