#!/bin/sh --
#
# Generate JSBSim table for CLq.
# row: Mach
#
# XXX Last-digit dependency on the cone in few Mach points.
#
set -e

subMs=$(ls -1 Tu-144D/cone_0.0)
superMs=$(ls -1 Tu-144D_equivalent/stability)

for M in $subMs; do
 printf $M
 awk 'NR==1 {printf "\t"$2"\n"}' Tu-144D/cone_0.0/$M/d_wbv
done |expand

for M in $superMs; do
 printf $M
 awk 'NR==1 {printf "\t"$2"\n"}' Tu-144D_equivalent/stability/$M/d_wbv
done |expand
