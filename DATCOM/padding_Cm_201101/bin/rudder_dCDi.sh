#!/bin/sh --
#
# Generate JSBSim table for rudder dCDi.
# row:    sideslip, deg
# column: deflection, deg
#
set -e


M=0.4
# SIC Insert first column with zero deflection.
awk 'NR==1 {printf "\t0.0\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\n"}'  Tu-144D-vtail/elevator/$M/CD |expand -t 10
awk 'NR!=1 {printf $1"\t0.0\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\n"}' Tu-144D-vtail/elevator/$M/CD |expand -t 10
