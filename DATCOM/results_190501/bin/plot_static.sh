#!/bin/sh --
#
# Prepare a plot of static coefficients vs Mach number, for choosing
# which Mach points to take.

grep -Ri '^ *6\.0' Tu-144D/cone_0.0/*/s_wb Tu-144D_equivalent/stability/*/s_wbv |sed -e's:Tu-144D/cone_0.0/::' -e's:Tu-144D_equivalent/stability/::' -e's;/.*:;;' |sort -g >static.txt
