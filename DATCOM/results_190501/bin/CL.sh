#!/bin/sh --
#
# Generate JSBSim table for CL.
# row:    AoA, deg.
# column: Mach
#
# XXX Last-digit dependency on the cone, looked random so I left it out.
#
set -e

TMPDIR=`mktemp -d`
trap "rm -rf ${TMPDIR:?}" 0 2 9 15

subMs=$(ls -1 Tu-144D/cone_0.0)
superMs=$(ls -1 Tu-144D_equivalent/stability)


nextM=
for M in $subMs; do
 if [ -z "$nextM" ]; then
  awk '{printf $1"\n"}' Tu-144D/cone_0.0/$M/s_wb >$TMPDIR/AoAs
 fi
 awk '{printf $3"\n"}' Tu-144D/cone_0.0/$M/s_wb >$TMPDIR/$M
 nextM=1
done

for M in $superMs; do
 awk '{printf $3"\n"}' Tu-144D_equivalent/stability/$M/s_wbv >$TMPDIR/$M
done

(
 cd $TMPDIR
 printf '\t'
 echo $subMs $superMs |sed -e's/  */	/g'
 paste AoAs $subMs $superMs
)| expand
