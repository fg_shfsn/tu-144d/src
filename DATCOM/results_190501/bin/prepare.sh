#!/bin/sh --
set -e

NALPHA=20
NBETA=9
ALT=10000.0
NELEVATOR=9
NAILERON=9
NRUDDERS=9
NRUDDERA=9

subMs='0.01 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.65 0.69'
superMs='1.1 1.2 1.25 1.3 1.35 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4'
controlMs='0.4'


for model in Tu-144D Tu-144D_equivalent; do
 if [ "$model" == Tu-144D_equivalent ]; then

  Ms=$superMs

  for M in $superMs; do
   dir="$model/stability/$M"
   mkdir -p "$dir"
   case="Tu-144D"
   ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M $ALT static 'WING-BODY-VERTICAL TAIL' >"$dir/s_wbv" &
   # FIXME Why in the fuck's name is alt suddenly 0?
   ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M 0.0 dynamic 'WING-BODY-VERTICAL TAIL' >"$dir/d_wbv" &
  done

 else

  Ms=$subMs

  for M in $subMs; do
   #for cone in 0.0 11.5 17.0; do
   for cone in 0.0 17.0; do
    dir="$model/cone_$cone/$M"
    mkdir -p "$dir"
    case="Tu-144D"
    if [ "$cone" != "0.0" ]; then
     case="$case cone $cone"
    fi
    ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M $ALT static 'WING-BODY-VERTICAL TAIL' >"$dir/s_wbv" &
    ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M $ALT static WING-BODY >"$dir/s_wb" &
    # FIXME Why in the fuck's name is alt suddenly 0?
    ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M 0.0 dynamic 'WING-BODY-VERTICAL TAIL' >"$dir/d_wbv" &
    ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M 0.0 dynamic WING-BODY >"$dir/d_wb" &
   done
  done

 fi

 for M in $controlMs; do
  dir="$model/elevator/$M"
  mkdir -p "$dir"
  case="Tu-144D"
  ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M $ALT symflp_dCm $NELEVATOR >"$dir/CL_Cm" &
  ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M $ALT symflp_dCDi $NELEVATOR >"$dir/CD" &
  dir="$model-vtail/elevator/$M"
  mkdir -p "$dir"
  case="Tu-144D vtail"
  ../bin/gimmedat.sh "$model-vtail/output.dat" "$case" $NBETA $M $ALT symflp_dCm $NRUDDERS >"$dir/CL_Cm" &
  ../bin/gimmedat.sh "$model-vtail/output.dat" "$case" $NBETA $M $ALT symflp_dCDi $NRUDDERS >"$dir/CD" &
  dir="$model/aileron/$M"
  mkdir -p "$dir"
  case="Tu-144D aileron"
  ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M $ALT asyflp_Cn $NAILERON >"$dir/Cn" &
  ../bin/gimmedat.sh "$model/output.dat" "$case" $NALPHA $M $ALT asyflp_Cl $NAILERON >"$dir/Cl" &
  dir="$model-vtail/aileron/$M"
  mkdir -p "$dir"
  case="Tu-144D vtail roll"
  ../bin/gimmedat.sh "$model-vtail/output.dat" "$case" $NBETA $M $ALT asyflp_Cn $NRUDDERA >"$dir/Cn" &
  ../bin/gimmedat.sh "$model-vtail/output.dat" "$case" $NBETA $M $ALT asyflp_Cl $NRUDDERA >"$dir/Cl" &
 done

done
