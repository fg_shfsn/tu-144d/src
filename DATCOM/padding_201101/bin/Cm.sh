#!/bin/sh --
#
# Generate JSBSim table for Cm.
# table:  cone, norm (cone, deg / 17 deg)
# row:    AoA, deg.
# column: Mach
#
# XXX Supersonic results do not include cone effects!
#
set -e

TMPDIR=`mktemp -d`
trap "rm -rf ${TMPDIR:?}" 0 2 9 15

cones="0.0 17.0"
subMs=$(ls -1 Tu-144D/cone_0.0)
superMs=$(ls -1 Tu-144D_equivalent/stability)


nextcone=
for cone in $cones; do

 nextM=
 for M in $subMs; do
  if [ -z "$nextM" ]; then
   awk '{printf $1"\n"}' Tu-144D/cone_$cone/$M/s_wb >$TMPDIR/AoAs
  fi
  awk '{printf $4"\n"}' Tu-144D/cone_$cone/$M/s_wb >$TMPDIR/$M
  nextM=1
 done

 for M in $superMs; do
  awk '{printf $4"\n"}' Tu-144D_equivalent/stability/$M/s_wbv >$TMPDIR/$M
 done

 (
  cd $TMPDIR
  echo '   <tableData breakPoint="'$(echo $cone | awk '{printf "%1.1f", $1/17.0}')'">'
  printf '\t'
# Append supersonic results to deflected nose table as well, to simplify FDM logic.
#  if [ -z "$nextcone" ]; then
   echo $subMs $superMs |sed -e's/  */	/g'
   paste AoAs $subMs $superMs
#  else
#   echo $subMs |sed -e's/  */	/g'
#   paste AoAs $subMs
#  fi
  echo '   </tableData>'
 )| expand

 nextcone=1
done
