#!/bin/sh --
#
# Generate JSBSim table for elevator dCDi.
# row:    AoA, deg
# column: deflection, deg
#
set -e


M=0.4
# SIC Get rid of the first column, because it is a dummy (see "TIPS ABOUT DATCOM BUGS" in README.TXT).
awk 'NR==1 {printf "\t"$2"\t"$3"\t"$4"\t"$5"\t0.0\t"$6"\t"$7"\t"$8"\t"$9"\n"}' Tu-144D/elevator/$M/CD |expand -t 10
awk 'NR!=1 {printf $1"\t"$3"\t"$4"\t"$5"\t"$6"\t0.0\t"$7"\t"$8"\t"$9"\t"$10"\n"}' Tu-144D/elevator/$M/CD |expand -t 10
