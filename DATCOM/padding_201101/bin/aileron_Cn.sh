#!/bin/sh --
#
# Generate JSBSim table for aileron Cn.
# row:    AoA, deg
# column: deflection difference, deg
#
set -e


M=0.4
awk 'NR==1 {printf "\t0.0\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\n"}' Tu-144D/aileron/$M/Cn |expand -t 11
awk 'NR!=1 {printf $1"\t0.0\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\n"}' Tu-144D/aileron/$M/Cn |expand -t 11
