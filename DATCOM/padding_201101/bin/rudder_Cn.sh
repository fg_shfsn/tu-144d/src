#!/bin/sh --
#
# Generate JSBSim table for rudder Cn.
# row:    AoA, deg
# column: deflection difference, deg
#
set -e


M=0.4
awk 'NR==1 {printf "\t0.0\t"0.5*$1"\t"0.5*$2"\t"0.5*$3"\t"0.5*$4"\t"0.5*$5"\t"0.5*$6"\t"0.5*$7"\t"0.5*$8"\t"0.5*$9"\n"}' Tu-144D-vtail/aileron/$M/Cn |expand -t 11
awk 'NR!=1 {printf $1"\t0.0\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\n"}' Tu-144D-vtail/aileron/$M/Cn |expand -t 11
