#!/bin/sh --
#
# Generate JSBSim table for elevator dCL.
# row:    deflection, deg
#
set -e


M=0.4
awk '{printf $1"\t"$2"\n"}' Tu-144D/elevator/$M/CL_Cm |sort -g |expand
