#!/bin/sh --
set -e

in=${1:?}
out=${2:?}

convert -background black -alpha remove -flip "$in" "$out-makeddstemp.tga"
nvcompress -color -bc1 "$out-makeddstemp.tga"
mv "$out-makeddstemp.dds" "$out"
rm -f "$out-makeddstemp.tga"
