#!/bin/sh --
set -e

in=${1:?}
out=${2:?}

convert -flip "$in" "$out-makeddstemp.tga"
nvcompress -alpha -bc3 "$out-makeddstemp.tga"
mv "$out-makeddstemp.dds" "$out"
rm -f "$out-makeddstemp.tga"
