#!/bin/sh --
set -e

sed -i.bak 's/sodipodi:insensitive="true"//g' ${@:?}
