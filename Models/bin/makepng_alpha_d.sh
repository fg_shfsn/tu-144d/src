#!/bin/sh --
set -e

in=${1:?}
out=${2:?}

convert "$in" "$out"
