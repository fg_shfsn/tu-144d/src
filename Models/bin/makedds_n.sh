#!/bin/sh --
set -e

in=${1:?}
out=${2:?}

convert -flip "$in" "$out-makeddstemp.tga"
mogrify -channel red -negate +channel -channel green -negate +channel "$out-makeddstemp.tga"
nvcompress -normal -bc5 "$out-makeddstemp.tga"
mv "$out-makeddstemp.dds" "$out"
rm -f "$out-makeddstemp.tga"
