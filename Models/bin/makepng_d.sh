#!/bin/sh --
set -e

in=${1:?}
out=${2:?}

convert -background black -alpha remove "$in" "$out"
